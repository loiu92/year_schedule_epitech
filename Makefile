DC=docker
PROJECT=year_schedule

include .env
export $(shell sed 's/=.*//' .env)

all: build start

re: clean build start

start:
	@echo "Launch attached project and build\n"
	@export $(shell sed 's/=.*//' .env)
	@$(DC) run -it -e AUTOLOGIN=$(AUTOLOGIN) -v $(PWD):/usr/src/app -v /usr/src/app/node_modules --name $(PROJECT) $(PROJECT)

build:
	@echo "Build Docker Image\n"
	@$(DC) build -t $(PROJECT) .
stop:
	@$(DC) stop $(PROJECT)
clean:
	@$(DC) rm -f $(PROJECT)
logs:
	@$(DC) logs -f $(PROJECT)

install_dc:
	curl https://get.docker.com | sh -


.PHONY: all stop clean
