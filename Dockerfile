FROM node:10-alpine
WORKDIR /usr/src/app

COPY package.* .

RUN yarn -i

CMD ["node", "index.js"]