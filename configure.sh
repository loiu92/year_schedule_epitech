#!/bin/sh

if [ ! -f ".env" ]; then
    echo "INIT PROJECT"
    read -p "Enter your autologin link (ex: auth-xxxx): " autologin;  echo "AUTOLOGIN="$$autologin > .env
    echo "Launch 'make' command to see your modules"
    echo "\n"
else
    echo "Already configured, delete .env file for init again"
fi
