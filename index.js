const axios = require('axios')
var AsciiTable = require('ascii-table')

var autologin = process.env.AUTOLOGIN;

var baseIntra = "https://intra.epitech.eu/"

var modules = [];

if (autologin === undefined)
{
    console.log("Autlogin undefined, make init and then add your autologin link in .env");
}

console.log("Get registered modules...");


axios.get(baseIntra + autologin + '/user/?format=json').then(function(data){
    axios.get(baseIntra + autologin + '/user/' + data.data.login + '/notes?format=json').then(function(user){
	var listModules = [];
	for (i = 0; i < user.data.modules.length; i++){
	    if (user.data.modules[i].scolaryear === new Date().getFullYear() || user.data.modules[i].scolaryear === new Date().getFullYear() - 1){
		console.log(user.data.modules[i].codemodule);
		listModules.push({codemodule: user.data.modules[i].codemodule, codeinstance: user.data.modules[i].codeinstance, scolaryear: user.data.modules[i].scolaryear});
	    }
	}
	console.log("\n" + listModules.length + " modules")
	console.log("\n\nGet modules info...\n");

	var table = new AsciiTable('Modules');

	table.setHeading('Grade', 'Student Credits', 'Default Credits', 'End Register', 'End Module', 'Title');

	table.setAlign(1, AsciiTable.RIGHT);
	table.setAlign(2, AsciiTable.RIGHT);
	
	var modules = [];
	var credits = 0;
	var total_now = 0;
	
	for (i = 0; i < listModules.length; i++){
	    axios.get(baseIntra + autologin + '/module/' + listModules[i].scolaryear + '/' + listModules[i].codemodule + '/' + listModules[i].codeinstance + '/?format=json').then(function(fetched){
		//TABLE
		if (fetched.data.student_credits == 0 && fetched.data.student_grade != "Echec")
		    table.addRow(fetched.data.student_grade, "N/A",                     fetched.data.credits, fetched.data.end_register, fetched.data.end, fetched.data.title);
		else
		    table.addRow(fetched.data.student_grade, fetched.data.student_credits, fetched.data.credits, fetched.data.end_register, fetched.data.end, fetched.data.title);
		//TOTAL
		if (fetched.data.student_credits == 0 && fetched.data.student_grade == "Echec")
		    credits += 0;
		else
		    credits += parseInt(fetched.data.user_credits);
		//SORT AND PRINT
		if (Object.keys(table.toJSON().rows).length == listModules.length){
		    table.sortColumn(3, function(a, b){
			return new Date(b) - new Date(a);
		    });
		    console.log(table.toString() + "\n");
		    console.log("Predictif Total: " + credits);
		}
	    }).catch(function(err){
		console.log(err);
	    });
	}	
    }).catch(function(err){
	console.log(err);
    });
}).catch(function(err){
    console.log(err);
});
