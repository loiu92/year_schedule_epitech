Requirements:
- `docker`
- `make`

After clone, just `./configure.sh`, copy paste your autologin (ex: auth-xxxx) and then `make`.

Init:
- `./configure.sh`

Launch:
- `make`

Launch Again:
- `make re`

Clean:
- `make clean`

Install Docker (with user sudo):
- `sudo make install_dc`

Examples Output:

```
ruellel@bluebox:~/Documents/intra_epitech$ make re
year_schedule
Build Docker Image

Sending build context to Docker daemon  931.3kB
Step 1/5 : FROM node:10-alpine
 ---> 9dfa73010b19
Step 2/5 : WORKDIR /usr/src/app
 ---> Using cache
 ---> 8a8dd85fcab4
Step 3/5 : COPY package.* .
 ---> Using cache
 ---> a071d473fc5c
Step 4/5 : RUN yarn -i
 ---> Using cache
 ---> e423849c221b
Step 5/5 : CMD ["node", "index.js"]
 ---> Using cache
 ---> 2b2f733dda40
Successfully built 2b2f733dda40
Successfully tagged year_schedule:latest
Launch attached project and build

Get registered modules...
G-EPI-000
M-EIP-701
M-INT-001
M-ADM-850
M-EIP-900
M-SEC-902
M-PJL-010
M-PRO-902
M-PRO-870
G-EPI-010
M-EIP-902
M-MOB-100
M-NCE-001
M-EIP-901
M-BYG-001
M-EIP-903
M-PRO-060
M-REA-901
M-IOT-200
M-WEB-901
M-TRV-002
M-PRO-045
M-TRV-006
M-RNE-001
M-ALG-102
M-REA-902
M-OPS-902

27 modules


Get modules info...

.----------------------------------------------------------------------------------------------------------------------------------------------------------.
|                                                                         Modules                                                                          |
|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| Grade  | Student Credits | Default Credits | End Register | End Module |                                      Title                                      |
|--------|-----------------|-----------------|--------------|------------|---------------------------------------------------------------------------------|
| N/A    |             N/A |               2 | 2020-03-03   | 2020-03-28 | M - Professional Efficiency (Tips and tricks to define your professional style) |
| N/A    |             N/A |               4 | 2020-02-25   | 2020-03-29 | M - DevOps                                                                      |
| N/A    |             N/A |               2 | 2020-02-18   | 2020-02-22 | M - Les clés du cadrage de projet                                               |
| N/A    |             N/A |               6 | 2020-02-11   | 2020-04-19 | M - React Native Mobile Development                                             |
| N/A    |             N/A |               4 | 2020-02-03   | 2020-02-28 | M - Cybersécu - gérer la sécurité applicative [Rennes]                          |
| N/A    |             N/A |               2 | 2020-01-28   | 2020-02-01 | M - Progressive Web Apps - Seminar                                              |
| N/A    |             N/A |               1 | 2020-01-14   | 2020-06-23 | S0 - Extra Curricular Engagement                                                |
| N/A    |             N/A |               3 | 2020-01-13   | 2020-02-15 | M - Intelligence Economique                                                     |
| N/A    |             N/A |               3 | 2020-01-07   | 2020-01-26 | M - Graphs algorithms and matching                                              |
| N/A    |             N/A |               5 | 2019-12-17   | 2020-03-01 | M - IoT                                                                         |
| N/A    |             N/A |              12 | 2019-12-14   | 2021-02-28 | M - Projets Libres                                                              |
| N/A    |             N/A |               1 | 2019-12-09   | 2019-12-13 | M - Shape your attitude                                                         |
| N/A    |             N/A |               2 | 2019-12-03   | 2019-12-07 | M - Marketing I                                                                 |
| N/A    |             N/A |               4 | 2019-11-26   | 2019-12-15 | M - Web Security                                                                |
| N/A    |             N/A |               4 | 2019-11-11   | 2019-12-21 | M - Initiation Kubernetes [Nice]                                                |
| N/A    |             N/A |               1 | 2019-11-04   | 2019-11-08 | M - Shape your internship !                                                     |
| N/A    |             N/A |               4 | 2019-11-03   | 2019-11-24 | M - Mobile Hybrid - Project                                                     |
| Echec  |               0 |               1 | 2019-10-08   | 2019-10-11 | M - How much does it cost?                                                      |
| A      |               6 |               6 | 2019-09-10   | 2019-11-10 | M - JavaScript Full Stack Development                                           |
| N/A    |             N/A |               0 | 2019-09-01   | 2021-08-31 | M - Administrative Meetings                                                     |
| N/A    |             N/A |               1 | 2019-09-01   | 2019-11-30 | M - EIP - Séminaire Devops                                                      |
| Acquis |               1 |               1 | 2019-09-01   | 2019-09-30 | EIP - Seminar - Pitch&Demo                                                      |
| N/A    |             N/A |               1 | 2019-09-01   | 2019-10-31 | M - EIP - Séminaire UI/UX                                                       |
| N/A    |             N/A |              15 | 2019-08-30   | 2020-02-26 | M3 - EIP / Epitech Innovative Project                                           |
| Acquis |             N/A |               0 | 2019-08-01   | 2019-09-09 | S0 - Spices                                                                     |
| Acquis |              60 |              60 | 2018-08-28   | 2018-08-27 | M - International                                                               |
| Acquis |              15 |              15 | 2018-08-28   | 2019-08-31 | M - EIP / Epitech Innovative Project 1&2                                        |
'----------------------------------------------------------------------------------------------------------------------------------------------------------'

Predictif Total: 111
```